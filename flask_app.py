# flask

from flask import Flask, render_template, request
from pony.orm import Database, select, db_session, Required

app = Flask(__name__)
db = Database()

class User(db.Entity):
    username = Required(str)
    password = Required(str)
    age = Required(int)

db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)


@app.route('/hello')
def hello():
    return "aaaaaaaaaaaaaaaaaaaaaaaa"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        u = request.form.get('username', 'some default value')
        p = request.form.get('password')
        a = request.form.get('age')
        User(username=u, password=p)
    else:
        return "Unrecognized request method."
        return f'Hello {u}, your password is {p}'

@app.route('/calculate', methods=['GET', 'POST'])
def calc():
    if request.method == 'GET':
        return render_template('index.html')
    elif request.method == 'POST':
        operations = {"multiply": "x", "divide": "/", "add": "+", "sub": "-"}

        num1 = int(request.form.get('num1'))
        num2 = int(request.form.get('num2'))
        action = request.form.get('action')
        result = 0
        if action == 'multiply':
            result = num1 * num2
        elif action == 'divide':
            result = num1 / num2
        elif action == 'sub':
            result = num1 - num2
        elif action == 'add':
            result = num1 + num2
        else:
            return "Unrecognized action."

        operation = operations[action]

        return render_template('index.html', ans=result, num1=num1, num2=num2, action=operation)